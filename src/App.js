import React, { Component } from 'react';
import './App.css';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { defaultFunction, defaultUser, editItem, removeItem, saveRecord } from './action';
import ListItem from './components/listIem';
import UserForm from './components/userForm';


class App extends Component {

  componentDidMount() {
    // this.props.fetchData();
    // this.props.getUser();
    const { defaultFunction, defaultUser } = this.props;
    defaultFunction();
    defaultUser();
  }

  render() {
    const { user_list, editItem, removeItem, saveRecord } = this.props;
    return (
      <div>
        <UserForm onSubmit={saveRecord} />
        {/* <div>{JSON.stringify(user_list) }</div> */}
        <ListItem dataList={user_list} deleteAction={removeItem} editAction={editItem}></ListItem>
        
      </div>
    );
  }
}

// function to convert the global state obtained from redux to local props
const mapStateToProps = (state) => ({
  user_list: state.account.user_list,
  user: state.account.user,
});

// const mapDispatchToProps = dispatch => ({
//   fetchData: () => dispatch(defaultFunction()),
//   editItemHandler: (data) => dispatch(editItem(data)),
//   removeItemHandler: (data) => dispatch(removeItem(data)),
//   saveData: (data) => dispatch(saveRecord(data)),
//   getUser: () => dispatch(defaultUser())
// });

const mapDispatchToProps =  dispatch => ({
  ...bindActionCreators({
    defaultFunction,
    defaultUser,
    editItem,
    removeItem,
    saveRecord,
  }, dispatch)
})

export default connect(mapStateToProps, mapDispatchToProps)(App);
