import { 
    FETCH_DATA, SET_USER, EDIT_ITEM, REMOVE_ITEM, SAVE_ITEM, UPDATE_RECORD,
    updateObjectInArray
} from '../action';

const initialState = {
    user_list: null,
    user: null,
};

const defaultReducer = (state = initialState, action) => {
    switch (action.type) {

        case FETCH_DATA:
            return Object.assign({}, state, { user_list: action.payload });

        case SET_USER:
            return Object.assign({}, state, { user: action.user });
        
        case EDIT_ITEM:
            return Object.assign({}, state, { user: action.user });

        case REMOVE_ITEM:
            return Object.assign({}, state, {
                user_list: state.user_list.filter(({ id }) => id !== action.payload.id)
            });
        
        case SAVE_ITEM:
            return Object.assign({}, state, { user_list: state.user_list.concat(action.formData) });
        
        case UPDATE_RECORD:
            let updatedList = updateObjectInArray(state.user_list, action.formData);
            return Object.assign({}, state, { user_list: updatedList });

        default:
            return state;
    }
};

export default defaultReducer;