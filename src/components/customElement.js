import React from 'react';
import 'react-widgets/dist/css/react-widgets.css';
import Multiselect from 'react-widgets/lib/Multiselect';
import DropdownList from 'react-widgets/lib/DropdownList';


export const renderMultiselect = ({ input, data, valueField, textField }) => (
    <Multiselect {...input}
        onBlur={() => input.onBlur()}
        value={input.value || []}
        data={data}
        valueField={valueField}
        textField={textField}
    />
);

export const renderDropdownList = ({ input, data, valueField, textField }) => (
    <DropdownList {...input}
        data={data}
        valueField={valueField}
        textField={textField}
        onChange={input.onChange} />
);

export const renderField = ({ input, label, type, cls, meta: { touched, error, warning } }) => (
    <div>
        <sub>{label}</sub>
        <input className={cls} {...input} placeholder={label} type={type} />
        {touched && ((error && <sup className="text-danger">{error}</sup>) || (warning && <span>{warning}</span>))}
    </div>
);