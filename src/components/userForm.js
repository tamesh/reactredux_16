import React from 'react';
import { Field, reduxForm, reset } from 'redux-form';
import { connect } from 'react-redux';
import { renderMultiselect, renderDropdownList, renderField } from './customElement';

const validate = values => {
    const errors = {}
    if (!values.userEmail) {
        errors.userEmail = 'Required';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.userEmail)) {
        errors.userEmail = 'Invalid email address';
    }
    if (!values.userPhone) {
        errors.userPhone = 'Required';
    } else if (!/^[0-9]/i.test(values.userPhone)) {
        errors.userPhone = 'Invalid Phone';
    }
    if (!values.userDepartment){
        errors.userDepartment = 'Required';
    }
    if (!values.userRole) {
        errors.userRole = 'Required';
    }
    return errors;
};
const maxLength = max => value =>
    value && value.length > max ? `Must be ${max} characters or less` : undefined;

let UserForm = (props) => {
    const { handleSubmit, pristine, submitting, valid, initialValues, onClearChanges } = props;
    const renderButton = () => {
        let button;

        if (initialValues && initialValues.id) {
            button = <button type="button" className="btn btn-success mr-4" onClick={handleSubmit} disabled={!valid || pristine || submitting}>Update</button>;
        } else {
            button = <button type="submit" className="btn btn-success mr-4" onClick={handleSubmit} disabled={!valid || pristine || submitting}>Submit</button>;
        }
        return button;
    };
    return (
        <div className="container">
            <div>
                <h2>User Form</h2>
                <form>
                    <div className="form-row">
                    <div className="form-group col-md-6">
                        <div className="">
                            <sub>Name</sub>
                            <Field autoFocus name="userName" component="input" type="text" className="form-control" />
                        </div>
                        <div className="">
                            <Field name="userEmail" type="email" component={renderField} label="Email" cls="form-control" />
                        </div>
                        <div className="">
                            <Field name="userPhone" label="Phone" component={renderField} type="phone" cls="form-control"
                                validate={[maxLength(10)]} />
                        </div>
                        <div className="">
                            <sub>Role</sub>
                            <Field name="userRole" component={renderDropdownList} type="text" className="form-control"
                                data={['Admin', 'User', 'Developer']}
                                valueField="value"
                                textField="color"
                            />
                        </div>
                    </div>
                    
                    <div className="form-group col-md-6">
                        <div>
                            <sub>Address</sub>
                            <Field name="userAdd" component="input" type="text" className="form-control" />
                        </div>
                        <div>
                            <sub>Notes</sub>
                            <Field name="userNotes" component="input" type="text" className="form-control" />
                        </div>
                        <div className="">
                            <sub>Department</sub>
                            <Field name="userDepartment" component={renderMultiselect} className="form-control"
                                data={[{ label: 'Admin', id: 'AD' }, { label: 'IT', id: 'IT' }, { label: 'Finance', id: 'FN' }]}
                                valueField="id"
                                textField="label"
                            />
                        </div>
                        <div className="">
                            <sub>Authorised</sub>
                            <Field name="userIsAdmin" component="input" type="checkbox" className="form-control" />
                        </div>
                    </div>
                    
                </div>
                    {renderButton()}
                    <button type="reset" className="btn btn-primary" onClick={onClearChanges}>Reset</button>
                </form>
            </div>
        </div>
    )
};

const resetForm = () => {
    return {
        type: 'SET_USER',
    };
};

const mapStateToProps = (state) => ({
    initialValues: state.account.user
});

const mapDispatchToProps = (dispatch) => ({
    onClearChanges: () => {
        dispatch(resetForm());
        dispatch(reset('userForm'));
    },
});

UserForm = reduxForm({
    form: 'userForm',
    enableReinitialize: true,
    onSubmitSuccess: (result, dispatch) => {
        dispatch(resetForm());
        dispatch(reset('userForm'));
    },
    validate,
})(UserForm);

UserForm = connect(mapStateToProps, mapDispatchToProps)(UserForm);

export default UserForm;