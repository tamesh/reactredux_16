import React from 'react';

const ListItem = ({ dataList, deleteAction, editAction }) => {
    return (
        <div className="container mt-5">
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Address</th>
                        <th scope="col">Notes</th>
                        <th scope="col">Role</th>
                        <th scope="col">Authorised</th>
                        <th scope="col">Department</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                {
                    dataList && dataList.map(item =>
                    <tr key={item.id}>
                        <td>
                            <h5>{item.title}</h5>
                            <span>{item.body}</span>
                        </td>
                        <td>
                            <div>{item.userAdd}</div>
                            <span> {item.userPhone} </span>
                        </td>
                        <td>{item.userNotes}</td>
                        <td>{item.userRole}</td>
                        <td>
                            <em className={(item.userIsAdmin)? 'text-success fa fa-unlock': 'text-danger fa fa-lock'}></em>
                        </td>
                        <td>
                            {/* <ul>
                            {
                                item.userDepartment.map( dep => 
                                <li key={dep.id}>
                                    { dep.label }
                                </li>)
                            }
                            </ul> */}
                        </td>
                        <td>
                            <em className="fa fa-pencil-square mr-3" onClick={() => editAction(item)}></em>
                            <em className="fa fa-trash" onClick={() => deleteAction(item)}></em>
                        </td>
                    </tr>
                )}
                </tbody>
                </table>
        </div>
    );
}


export default ListItem;