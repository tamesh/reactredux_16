export const FETCH_DATA = 'FETCH_DATA';
export const SET_USER = 'SET_USER';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const EDIT_ITEM = 'EDIT_ITEM';
export const SAVE_ITEM = 'SAVE_ITEM';
export const UPDATE_RECORD = 'UPDATE_RECORD';

// default function to display redux action format
export const defaultFunction = () => {
    // let initialData = [{
    //     id: uuidv4(),
    //     userName: 'Tameshwar Nirmalkar',
    //     userAdd: '300, abc, Baner',
    //     userPhone: 1239073847,
    //     userEmail: 'test@test.com',
    //     userNotes: 'This is sample notes',
    //     userIsAdmin: false,
    //     userRole: 'User',
    //     userDepartment: [{id:'AD', label: 'Admin'}, {id:'IT', label: 'IT'}],
    // }, {
    //     id: uuidv4(),
    //     userName: 'Sam Dicosta',
    //     userAdd: '201, Aundh',
    //     userPhone: 1121234232,
    //     userEmail: 'sd@test.com',
    //     userNotes: 'This is Sam dicosta notes',
    //     userIsAdmin: true,
    //     userRole: 'Admin',
    //     userDepartment: [{ id: 'FN', label: 'Finance' }, { id: 'IT', label: 'IT' }],
    // }];
    return dispatch => {
        fetch('https://jsonplaceholder.typicode.com/posts').then(response => response.json())
        .then(res => {
            dispatch({
                type: FETCH_DATA,
                payload: res
            });
        })
    }
    
}

export const defaultUser = () => {
    let initialData = {
        id: null,
        userName:  null,
        userAdd: null,
        userPhone: null,
        userEmail: null,
        userNotes: null,
        userIsAdmin: null,
        userRole: null,
        userDepartment: null,
        isEditable: null,
    };
    
    return {
        type: SET_USER,
        user: initialData,
    };
}

export const uuidv4 = () => {
    return ([1e7] + 1e3 + 4e3 + 8e3 + 1e11).replace(/[018]/g, c =>
        // eslint-disable-next-line no-mixed-operators
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
}

export const removeItem = (data) => {
    return {
        type: REMOVE_ITEM,
        payload:data,
    };
}

export const editItem = (user) => {
    console.log('Item: ', user );
    return {
        type: EDIT_ITEM,
        user,
    };
}

export const saveRecord = (values) => {
    if (values.id){
        return {
            type: UPDATE_RECORD,
            formData: values
        }
    } else {
        return {
            type: SAVE_ITEM,
            formData: Object.assign({}, values, { id: uuidv4()} ),
        }
    };
}

export const updateObjectInArray = (array, data) => {
    return array.map((item, index) => {
        if (item.id !== data.id) {
            return item;
        }
        return {
            ...item,
            ...data
        }
    })
}